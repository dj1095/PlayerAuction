package com.mindtree.playerauction.exceptions;

/**
 * @author M1044441 The NotABatsmanException wraps the standard java exceptions
 *         and enriches them with a custom error code
 * 
 */

public class NotABatsmanException extends IllegalArgumentException {

	/**
	 * @param message
	 *            a custom message that will be appropriate for this exception
	 * @param cause
	 *            a throwable reference which contains the details about the cause
	 *            of exception
	 */
	public NotABatsmanException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotABatsmanException() {
		super();

	}

	public NotABatsmanException(String arg0) {
		super(arg0);

	}

}
