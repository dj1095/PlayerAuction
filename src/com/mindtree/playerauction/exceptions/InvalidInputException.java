/**
 * 
 */
package com.mindtree.playerauction.exceptions;

import java.util.InputMismatchException;

/**
 * @author M1044441 The InvalidInputException wraps the standard java exceptions
 *         and enriches them with a custom error code
 * 
 */
public class InvalidInputException extends InputMismatchException {
	/**
	 * @param message
	 *            a custom message that will be appropriate for this exception
	 * @param cause
	 *            a throwable reference which contains the details about the cause
	 *            of exception
	 */

	public InvalidInputException(String message, Throwable cause) {
		super(message);
	}

	public InvalidInputException() {
		super();

	}

	public InvalidInputException(String arg0) {
		super(arg0);

	}

}
