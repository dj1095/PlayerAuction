package com.mindtree.playerauction.exceptions;

/**
 * @author M1044441 The DuplicateEntryException wraps the standard java
 *         exceptions and enriches them with a custom error code
 * 
 */
public class DuplicateEntryException extends IllegalArgumentException {
	/**
	 * @param message
	 *            a custom message that will be appropriate for this exception
	 * @param cause
	 *            a throwable reference which contains the details about the cause
	 *            of exception
	 */

	public DuplicateEntryException(String message, Throwable cause) {
		super(message);
	}

	public DuplicateEntryException() {
		super();

	}

	public DuplicateEntryException(String arg0) {
		super(arg0);

	}

}
