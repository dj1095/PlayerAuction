package com.mindtree.playerauction.exceptions;

/**
 * @author M1044441 The InvalidCategoryException wraps the standard java
 *         exceptions and enriches them with a custom error code
 * 
 */
public class InvalidCategoryException extends IllegalArgumentException {

	public InvalidCategoryException() {
		super();

	}

	/**
	 * @param message
	 *            a custom message that will be appropriate for this exception
	 * @param cause
	 *            a throwable reference which contains the details about the cause
	 *            of exception
	 */

	public InvalidCategoryException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidCategoryException(String arg0) {
		super(arg0);

	}

}
