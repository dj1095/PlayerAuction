package com.mindtree.playerauction.exceptions;

/**
 * @author M1044441 The InvalidTeamNameException wraps the standard java
 *         exceptions and enriches them with a custom error code
 * 
 */
public class InvalidTeamNameException extends IllegalArgumentException {

	/**
	 * @param message
	 *            a custom message that will be appropriate for this exception
	 * @param cause
	 *            a throwable reference which contains the details about the cause
	 *            of exception
	 */
	public InvalidTeamNameException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidTeamNameException() {
		super();

	}

	public InvalidTeamNameException(String arg0) {
		super(arg0);

	}

}
