package com.mindtree.playerauction.manager.impl;

import java.sql.SQLException;
import java.util.List;

import com.mindtree.playerauction.dao.PlayerDao;
import com.mindtree.playerauction.dao.impl.PlayerDaoImpl;
import com.mindtree.playerauction.entity.Player;
import com.mindtree.playerauction.exceptions.InvalidCategoryException;
import com.mindtree.playerauction.exceptions.InvalidTeamNameException;
import com.mindtree.playerauction.exceptions.NotABatsmanException;
import com.mindtree.playerauction.manager.Manager;

/**
 * @author M1044441 PlayerAuctionManagerImpl class will validate all the
 *         business validations given and tries to add a player ,displayaPlayers
 *         and exit from the app
 */
public class PlayerAuctionManagerImpl implements Manager {

	private PlayerDao dao = new PlayerDaoImpl();

	/**
	 * @param player
	 *            a player object(plain old java object) which contains all the
	 *            details of player which includes name,category
	 *            ,team,highScore,bestFigure
	 * 
	 *            this method tries to add a player to the database after evaluating
	 *            all business validations
	 */
	public void addPlayer(Player player) {

		try {
			if (!(player.getCategory().equalsIgnoreCase("batsman") || player.getCategory().equalsIgnoreCase("bowler")
					|| player.getCategory().equalsIgnoreCase("allrounder")))
				throw new InvalidCategoryException("Invalid category name, please check your input");

			if ((player.getCategory().equalsIgnoreCase("batsman")
					&& (player.getHighestScore() <= 50 || player.getHighestScore() >= 200)))
				throw new NotABatsmanException("Invalid batsman name, please check your input");

			if (PlayerDaoImpl.isTeamPresent(player.getTeamName()) == -1)
				throw new InvalidTeamNameException("Invalid team name, please check your input");

			dao.insert(player);

		} catch (SQLException e3) {
			System.out.println(e3.getMessage());
		} catch (InvalidTeamNameException e) {
			System.out.println(e.getMessage());
		} catch (InvalidCategoryException e) {
			System.out.println(e.getMessage());
		} catch (NotABatsmanException e) {
			System.out.println(e.getMessage());

		}
	}

	/**
	 * @param teamName
	 *            a string value which should contain the teamName from which the
	 *            players will be displayed
	 * 
	 *            this method will display all the specified team members along with
	 *            their name and category
	 */
	public void displayPlayers(String teamName) {
		List<Player> listofPlayers;
		try {
			if (PlayerDaoImpl.isTeamPresent(teamName) == -1)
				throw new InvalidTeamNameException("Invalid team name, please check your input");
			listofPlayers = dao.display(teamName);
			if (listofPlayers == null)
				System.out.println("NO Results Found");
			else {
				for (Player obj : listofPlayers) {
					System.out.print(obj.getPlayerName() + "\t");
					System.out.println(obj.getCategory());
				}
			}
		} catch (SQLException e) {
			System.out.println("Exception occured while displaying");
			return;
		} catch (InvalidTeamNameException e) {
			System.out.println(e.getMessage());
			return;
		}
	}

	/**
	 * this method exits from the menu and closes all the opened resources
	 */
	public void exit() {

		try {
			dao.closeConnections();
		} catch (SQLException e) {
			System.out.println("error occured while closing connections");
			return;
		}

	}

}