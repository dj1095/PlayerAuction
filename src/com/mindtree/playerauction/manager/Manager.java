package com.mindtree.playerauction.manager;

import com.mindtree.playerauction.entity.Player;

public interface Manager {

	public void addPlayer(Player object);

	public void displayPlayers(String teamName);

	public void exit();
}
