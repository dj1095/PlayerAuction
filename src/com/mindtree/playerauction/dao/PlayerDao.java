package com.mindtree.playerauction.dao;

import java.sql.SQLException;
import java.util.List;

import com.mindtree.playerauction.entity.Player;

public interface PlayerDao {

	public void insert(Player obj) throws SQLException;

	public List<Player> display(String playerName) throws SQLException;

	public void closeConnections() throws SQLException;
}
