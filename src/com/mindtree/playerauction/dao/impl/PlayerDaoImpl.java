package com.mindtree.playerauction.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mindtree.playerauction.dao.PlayerDao;
import com.mindtree.playerauction.entity.Player;
import com.mindtree.playerauction.util.DBConnect;

public class PlayerDaoImpl implements PlayerDao {

	static Connection con;
	static ResultSet rs;
	public static PreparedStatement ps;

	/**
	 * @param teamName
	 * @return integer that specifies team_id present in the database and -1 for the
	 *         team that is not present in the db
	 * 
	 * @throws SQLException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static int isTeamPresent(String teamName) throws SQLException {
		if (con == null)
			con = DBConnect.connectToDb();
		ps = con.prepareStatement("SELECT team_id FROM team WHERE team_name=?");
		ps.setString(1, teamName);
		rs = ps.executeQuery();
		if (rs.next())
			return rs.getInt("team_id");
		return -1;
	}

	@Override
	public void insert(Player obj) throws SQLException {
		try {
			if (con == null)
				con = DBConnect.connectToDb();
			ps = con.prepareStatement(
					"INSERT INTO player(player_name,category,highestscore,bestfigure) VALUES(?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, obj.getPlayerName());
			ps.setString(2, obj.getCategory());
			ps.setInt(3, obj.getHighestScore());
			ps.setString(4, obj.getBestFigure());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			int autoIncVal = -1;
			if (rs.next())
				autoIncVal = rs.getInt(1);

			int teamId = isTeamPresent(obj.getTeamName());
			ps = con.prepareStatement("INSERT INTO team_player(player_no,team_id) VALUES(?,?)");
			ps.setInt(1, autoIncVal);
			ps.setInt(2, teamId);
			ps.executeUpdate();
			System.out.println("Player added Succesfully at playerNumber :" + autoIncVal);
		} finally {
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();
			if (con != null)
				con.close();
		}
	}

	@Override
	public List<Player> display(String teamName) throws SQLException {

		try {
			teamName = teamName.toUpperCase();
			if (con == null)
				con = DBConnect.connectToDb();
			ps = con.prepareStatement("\r\n"
					+ "	 SELECT PLAYER_NAME,CATEGORY FROM PLAYER,TEAM_PLAYER WHERE TEAM_ID=(SELECT TEAM_ID FROM TEAM WHERE TEAM_NAME=?) AND\r\n"
					+ "     TEAM_PLAYER.PLAYER_NO=PLAYER.PLAYER_NO;");
			ps.setString(1, teamName);
			rs = ps.executeQuery();
			ArrayList<Player> listOfPlayers = null;
			if (!rs.next())
				return listOfPlayers;
			else {
				listOfPlayers = new ArrayList<Player>();
				do {
					Player obj = new Player();
					obj.setPlayerName(rs.getString("player_name"));
					obj.setCategory(rs.getString("category"));
					listOfPlayers.add(obj);
				} while (rs.next());
			}
			return listOfPlayers;
		} finally {
			if (ps != null)
				ps.close();
			if (rs != null)
				rs.close();
			if (con != null)
				con.close();
		}

	}

	@Override
	public void closeConnections() throws SQLException {
		try {
			DBConnect.releaseResources();
		} finally {
			if (ps != null)
				ps.close();
			if (rs != null)
				rs.close();
			if (con != null)
				con.close();
		}
	}

}
