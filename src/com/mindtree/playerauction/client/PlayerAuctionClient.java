package com.mindtree.playerauction.client;

import java.util.Scanner;

import com.mindtree.playerauction.entity.Player;
import com.mindtree.playerauction.manager.Manager;
import com.mindtree.playerauction.manager.impl.PlayerAuctionManagerImpl;

public class PlayerAuctionClient {

	private static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {

		Manager manager = null;
		boolean valid = true;
		do {

			try {

				System.out.println("Enter the preffered choice \n 1.AddPlayers \n 2.Dispay \n 3.Exit");
				int choice = Integer.parseInt(in.nextLine().trim());
				manager = new PlayerAuctionManagerImpl();
				switch (choice) {
				case 1: {

					Player obj = new Player();
					System.out.print("Enter Player Name :");
					String playerName = in.nextLine().trim();
					obj.setPlayerName(playerName);

					System.out.print("\nEnter category :");
					String category = in.nextLine().trim();
					obj.setCategory(category);

					System.out.print("\nEnter Highest Score :");
					Integer highestScore = Integer.parseInt(in.nextLine().trim());
					obj.setHighestScore(highestScore);

					System.out.print("\nEnter Best Figure :");
					String bestFigure = in.nextLine().trim();
					obj.setBestFigure(bestFigure);

					System.out.print("\n Enter the teamName");
					String teamName = in.nextLine().trim();
					obj.setTeamName(teamName);
					manager.addPlayer(obj);
					break;
				}
				case 2:
					System.out.println("Enter the TeamName");
					String TeamName = in.nextLine().trim();
					manager.displayPlayers(TeamName);
					break;
				case 3:
					System.out.println("Thank you...bye bye..");
					manager.exit();
					valid = false;
					break;
				default:
					System.err.println("Invalid Option...");
					break;
				}
			} catch (NumberFormatException e) {
				System.out.println("Invalid Input given");
				valid = true;
			}

		} while (valid);

	}
}
