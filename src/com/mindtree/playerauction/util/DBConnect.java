package com.mindtree.playerauction.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnect {

	private DBConnect() {

	}

	static Connection con = null;

	public static Connection connectToDb() throws SQLException {

		Properties prop = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(
					"C:\\Users\\M1044441\\eclipse-durgapramodh\\PlayerAuctionApplication\\src\\com\\mindtree\\playerauction\\util\\DB.properties");
			prop.load(fis);
		} catch (FileNotFoundException e) {
			System.out.println("problem while connecting to database.please check the dbconnection");
		} catch (IOException e) {
			System.out.println("problem while connecting to database");
		}

		String dburl = prop.getProperty("dburl");
		String user = prop.getProperty("user");
		String password = prop.getProperty("password");

		con = DriverManager.getConnection(dburl, user, password);
		return con;
	}

	public static void releaseResources() {

		try {
			if (con != null)
				con.close();

		} catch (SQLException e) {
			System.out.println("Error while releasing resources");
		}

	}

}
